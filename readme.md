Using ansible+vagrant, the nvidia hardware is accessible to the VM. Notice the use of "KVM_hidden", otherwise the graphics card will be unusable


After installing the drivers, X fails to start, so we need to disable X on the nvidia hardware.

use nvidia-smi to check to performance of the device.

the vagrant password is not reset, so you need to go into the box `vagrant ssh` and issue a `sudo passwd vagrant`

Refs

* https://wiki.debian.org/NvidiaGraphicsDrivers
* https://www.node35.com/restart-ubuntu-x-window-server-from-command-line/
* https://xcat-docs.readthedocs.io/en/stable/advanced/gpu/nvidia/verify_cuda_install.html

